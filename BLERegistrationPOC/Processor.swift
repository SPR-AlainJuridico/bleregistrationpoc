//
//  Processor.swift
//  BLERegistrationPOC
//
//  Created by Alain Juridico on 9/1/17.
//  Copyright © 2017 SPR Inc. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit
import UserNotifications

public class Processor: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var centralManager: CBCentralManager!
    var deviceUUID: String?
    var serviceUUID: String?
    var charateristicUUID: String?
    var discoveredPeripheral: CBPeripheral?
    let centralManagerIdentifier: String = "centralManagerIdentifier"
    var bleDelegate: BLEDelegate?
    var lastNotificationSent: Date?
    let deviceName: String = "Adafruit Bluefruit LE"
    
    override init() {
        super.init()
        self.centralManager = CBCentralManager(delegate: self,
                                               queue: nil,
                                               options: [CBCentralManagerOptionRestoreIdentifierKey:self.centralManagerIdentifier])
        
    }
    
    func startScanning(deviceID: String, service: String, characteristic: String, delegate: BLEDelegate) {
        self.serviceUUID = service
        self.charateristicUUID = characteristic
        self.deviceUUID = deviceID
        self.bleDelegate = delegate
        
        if let device = self.discoveredPeripheral {
            self.centralManager.connect(device, options: nil)
        } else {
            guard let serviceID = self.serviceUUID else { return }
            if self.centralManager.state == .poweredOn {
                self.centralManager.scanForPeripherals(withServices: [CBUUID(string: serviceID)],
                                                       options: [CBCentralManagerScanOptionAllowDuplicatesKey:1])
            }
        }
    }
    
    public func stopScanning() {
        self.centralManager.stopScan()
    }
    
    public func disconnect() {
        if let device = self.discoveredPeripheral {
            self.centralManager.cancelPeripheralConnection(device)
        }
    }
    
    // MARK: - CBCentralManagerDelegate
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if (central.state == .poweredOn) {
            NSLog("Central Manager ready");
            if let peripheral = self.discoveredPeripheral {
                self.centralManager.connect(peripheral, options: nil)
            } else {
                guard let deviceID = self.deviceUUID,
                    let serviceID = serviceUUID,
                    let characteristicID = charateristicUUID,
                    let delegate = self.bleDelegate else {
                        return
                }
                self.startScanning(deviceID: deviceID,
                                   service: serviceID,
                                   characteristic: characteristicID,
                                   delegate: delegate)
            }
        } else {
            NSLog("Central Manager is not ready")
        }
    }
    
    public func centralManager(_ central: CBCentralManager,
                               didDiscover peripheral: CBPeripheral,
                               advertisementData: [String : Any],
                               rssi RSSI: NSNumber) {
        NSLog("*DID DISCOVER PERIPHERAL*: \(peripheral)")
        if peripheral.name == deviceName && (self.discoveredPeripheral == nil) {
            
            self.stopScanning()
            self.discoveredPeripheral = peripheral
            self.centralManager.connect(peripheral, options: nil)
        }
    }
    
    public func centralManager(_ central: CBCentralManager,
                               didConnect peripheral: CBPeripheral) {
        NSLog("*DID CONNECT TO PERIPHERAL*: \(peripheral)")
        guard let serviceID = self.serviceUUID else { return }
        if let delegate = self.bleDelegate, let deviceName = peripheral.name {
            delegate.connectedToDevice(deviceName: deviceName)
        }
        peripheral.delegate = self
        peripheral.discoverServices([CBUUID(string: serviceID)])
    }
    
    public func centralManager(_ central: CBCentralManager,
                               didFailToConnect peripheral: CBPeripheral,
                               error: Error?) {
        NSLog("Failed to connect to peripheral with error: \(String(describing: error))")
    }
    
    public func centralManager(_ central: CBCentralManager,
                               didDisconnectPeripheral peripheral: CBPeripheral,
                               error: Error?) {
        NSLog("*DID DISCONNECT FROM PERIPHERAL*: \(peripheral)")
        self.clearLocalConnectedDeviceInfo()
        self.discoveredPeripheral = nil
        if let delegate = self.bleDelegate {
            delegate.didDisconnect()
        }
    }
    
    public func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
        NSLog("centralManager:willRestoreState data: \(dict)")
        if let peripheralsObject = dict[CBCentralManagerRestoredStatePeripheralsKey] {
            let peripherals = peripheralsObject as! Array<CBPeripheral>
            for device in peripherals {
                if device.name == self.deviceName {
                    device.delegate = self
                    self.discoveredPeripheral = device
                    break
                }
            }
        }
    }
    
    // MARK: - CBPeripheralDelegate
    
    public func peripheral(_ peripheral: CBPeripheral,
                           didDiscoverServices error: Error?) {
        if let discoveryError = error {
            NSLog("Error discovering services: \(discoveryError.localizedDescription)")
            return
        } else {
             NSLog("*DID DISCOVER SERVICES* for peripheral: \(peripheral)")
            if let services = peripheral.services, let service = services.first {
                guard let characteristicID = self.charateristicUUID else { return }
                peripheral.delegate = self
                peripheral.discoverCharacteristics([CBUUID(string: characteristicID)], for: service)
            }
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral,
                           didDiscoverCharacteristicsFor service: CBService,
                           error: Error?) {
        
        if let discoveryError = error {
            NSLog("Error discovering characteristics: \(discoveryError.localizedDescription)")
        } else {
            guard let services = peripheral.services,
                let service = services.first,
                let characteristics = service.characteristics,
                let characteristic = characteristics.first else {
                return
            }
            
            NSLog("*DID DISCOVER CHARACTERISTICS*: \(peripheral)")
            NSLog("Characteristic value: \(String(describing: characteristic.value))")
            
            peripheral.delegate = self
            if (characteristic.properties.contains(.notify) || characteristic.properties.contains(.indicate)) {
                peripheral.setNotifyValue(true, for: characteristic)
            }
            
            self.discoveredPeripheral = peripheral
            self.storeConnectedDeviceInfo()
            
            // This is an attempt to get the current value after connecting.
            // It looks like the characteristic of the Adafruit Bluefruit LE
            // always returns nil after first connect. Not sure if this has to do
            // with this particular device
            if let val = characteristic.value {
                let stringVal = String(bytes: val, encoding: .utf8) ?? "No Value"
                let userDefaults = UserDefaults.standard
                userDefaults.set(stringVal, forKey: Constants.lastCharacteristicValueKey)
                if let delegate = self.bleDelegate {
                    delegate.valueChanged(newValue: stringVal)
                }
            }
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral,
                           didUpdateValueFor characteristic: CBCharacteristic,
                           error: Error?) {
        NSLog("*DID UPDATE VALUE* for characteristic: \(characteristic)")
        if let val = characteristic.value {
            let stringVal = String(bytes: val, encoding: .utf8) ?? "No Value"
            NSLog("Characteristic value: \(stringVal)")
            self.saveLastCharacteristicValueRecieved(value: stringVal)
            self.saveLastBLENotificationReceivedDate()
            
            // Only send local notification if notifications have been granted
            // by the user and application is NOT active
            // i.e. in background/terminated etc.
            let application = UIApplication.shared
            let appDelegate = application.delegate as? AppDelegate
            if (appDelegate?.userGrantedNotifications())! {
                let state = UIApplication.shared.applicationState
                if state != .active {
                    // Supress notifications to 10 second intervals
                    if let lastSent = self.lastNotificationSent {
                        if abs(lastSent.timeIntervalSinceNow) > 10 {
                            self.sendLocalNotification(value: stringVal)
                            self.lastNotificationSent = Date()
                        }
                    } else {
                        self.sendLocalNotification(value: stringVal)
                        self.lastNotificationSent = Date()
                    }
                }
            }

            if let delegate = self.bleDelegate {
                delegate.valueChanged(newValue: stringVal)
            }
        } else {
            self.saveLastCharacteristicValueRecieved(value: "No Value")
            if let delegate = self.bleDelegate {
                delegate.valueChanged(newValue: "No Value")
            }
        }
        // TODO: Queue data to be sent
    }
    
    public func peripheral(_ peripheral: CBPeripheral,
                           didWriteValueFor characteristic: CBCharacteristic,
                           error: Error?) {
        NSLog("*DID WRITE VALUE* for characteristic: \(characteristic)")
        NSLog("Characteristic value: \(String(describing: characteristic.value))")
    }
    
    public func peripheral(_ peripheral: CBPeripheral,
                           didUpdateNotificationStateFor characteristic: CBCharacteristic,
                           error: Error?) {
        if let val = characteristic.value {
            NSLog("*DID UPDATE NOTIFICATION STATE* for characteristic: \(characteristic)")
            NSLog("Characteristic value: \(String(describing: characteristic.value))")
            let stringVal = String(bytes: val, encoding: .utf8) ?? "No Value"
            let userDefaults = UserDefaults.standard
            userDefaults.set(stringVal, forKey: Constants.lastCharacteristicValueKey)
        }
    }
    
    private func storeConnectedDeviceInfo() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(self.deviceUUID, forKey: Constants.deviceIDKey)
        userDefaults.set(self.serviceUUID, forKey: Constants.serviceIDKey)
        userDefaults.set(self.charateristicUUID, forKey: Constants.characteristicIDKey)
    }
    
    private func clearLocalConnectedDeviceInfo() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: Constants.deviceIDKey)
        userDefaults.removeObject(forKey: Constants.serviceIDKey)
        userDefaults.removeObject(forKey: Constants.characteristicIDKey)
        userDefaults.removeObject(forKey: Constants.lastCharacteristicValueKey)
        userDefaults.removeObject(forKey: Constants.lastNotificationReceivedDateKey)
    }
    
    private func saveLastBLENotificationReceivedDate() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(Date(), forKey: Constants.lastNotificationReceivedDateKey)
    }
    
    private func saveLastCharacteristicValueRecieved(value: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey: Constants.lastCharacteristicValueKey)
    }
    
    private func sendLocalNotification(value: String) {
        let titleText = "BLE Notification Received"
        let bodyText = "New characteristic value: \(value)"
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.title = titleText
            content.subtitle = ""
            content.body = bodyText
            content.badge = 1
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.5,
                                                            repeats: false)
            
            let requestIdentifier = "CharacteristicValueChanged"
            let request = UNNotificationRequest(identifier: requestIdentifier,
                                                content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request,
                                                   withCompletionHandler: { (error) in
                                                    // Handle error
                                                    if error != nil {
//                                                        self.logText(text: "Error Adding request to UNUserNotificationCenter")
                                                    }
            })
        } else {
            let notification = UILocalNotification()
            notification.fireDate = Date()
            notification.alertTitle = titleText
            notification.alertBody = bodyText
            notification.alertAction = nil
            notification.hasAction = false
            notification.userInfo = nil
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }
}

