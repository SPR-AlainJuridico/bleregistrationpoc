//
//  ViewController.swift
//  BLERegistrationPOC
//
//  Created by Alain Juridico on 9/1/17.
//  Copyright © 2017 SPR Inc. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, BLEDelegate {

    let deviceIDString = "51CAB63F-714D-4893-A358-73A09A8655B9"
    let serviceIDString = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
    let characteristicIDString = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
    
    var bleBroker: BLEBroker? = (UIApplication.shared.delegate as? AppDelegate)?.bleBroker
    
    @IBOutlet weak var connectionLabel: UILabel!
    @IBOutlet weak var characteristicValueLabel: UILabel!
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var lastLaunchDateLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        self.bleBroker?.disconnect()
        self.connectionLabel.text = "Disconnected"
        self.characteristicValueLabel.text = "Last value: \(UserDefaults.standard.string(forKey: Constants.lastCharacteristicValueKey) ?? "")"
        self.disconnectButton.setTitle("Connect", for: .normal)
        self.updateLastBLEReceiveDateText()
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if appDelegate != nil, let broker = appDelegate?.bleBroker {
            self.bleBroker = broker
            self.reconnectIfNecessary()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // IBAction
    @IBAction func disconnectTapped(_ sender: Any) {
        if let broker = self.bleBroker {
            if self.disconnectButton.titleLabel?.text == "Connect" {
                let message = BLEMessage(topic: deviceIDString + "/" + serviceIDString + "/" + characteristicIDString)
                self.activityIndicator.startAnimating()
                broker.subscribe(message: message, delegate: self)
            } else {
                broker.disconnect()
            }
        }
    }
    
    // Value BLEDelegate
    func connectedToDevice(deviceName: String) {
        self.activityIndicator.stopAnimating()
        self.connectionLabel.text = "Connected to device: \(deviceName)"
        self.disconnectButton.setTitle("Disconnect", for: .normal)
    }
    
    func didDisconnect() {
        self.connectionLabel.text = "Disconnected"
        self.characteristicValueLabel.text = "Last value: \(UserDefaults.standard.string(forKey: Constants.lastCharacteristicValueKey) ?? "")"
        self.disconnectButton.setTitle("Connect", for: .normal)
    }
    
    func valueChanged(newValue: String) {
        // Present new value
        self.characteristicValueLabel.text = "Last Value: " + newValue
        self.updateLastBLEReceiveDateText()
    }
    
    // Private
    private func reconnectIfNecessary() {
        // We want to reconnect if we have saved info from the previous connection
        // This means that we were previously connected to a device and the
        // disconnection was involuntary(app manually terminated or terminated
        // by the OS)
        let userDefaults = UserDefaults.standard
        if let deviceID = userDefaults.string(forKey: Constants.deviceIDKey),
            let serviceID = userDefaults.string(forKey: Constants.serviceIDKey),
            let characteristicID = userDefaults.string(forKey: Constants.characteristicIDKey) {
            self.activityIndicator.startAnimating()
            let message = BLEMessage(topic: deviceID + "/" + serviceID + "/" + characteristicID)
            self.bleBroker?.subscribe(message: message, delegate: self)
        }
    }
    
    private func updateLastBLEReceiveDateText() {
        if let lastNotificationReceivedDate = UserDefaults.standard.value(forKey: Constants.lastNotificationReceivedDateKey) as? Date {
            self.lastLaunchDateLabel.text = "Interval since last BLE: \(lastNotificationReceivedDate)"
        } else {
            self.lastLaunchDateLabel.text = "Interval since last BLE: ---"
        }
    }
}

