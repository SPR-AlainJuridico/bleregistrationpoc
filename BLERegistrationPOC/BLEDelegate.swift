//
//  BLEDelegate.swift
//  BLERegistrationPOC
//
//  Created by Alain Juridico on 9/11/17.
//  Copyright © 2017 SPR Inc. All rights reserved.
//

import Foundation
import CoreBluetooth

public protocol BLEDelegate {
    func connectedToDevice(deviceName: String)
    func didDisconnect()
    func valueChanged(newValue: String)
}
