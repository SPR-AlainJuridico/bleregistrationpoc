//
//  BLEBroker.swift
//  BLERegistrationPOC
//
//  Created by Alain Juridico on 9/5/17.
//  Copyright © 2017 SPR Inc. All rights reserved.
//

import Foundation
import UIKit

public struct BLEMessage {
    // topic format: /(device ID)/(service ID)/(characteristic ID)
    var topic: String
}

public class BLEBroker {
    
    private var processor: Processor = Processor()
    
    public func subscribe(message: BLEMessage, delegate: BLEDelegate) {
        
        var deviceID: String
        var serviceID: String
        var characteristicID: String
        
        let topicComponents = message.topic.components(separatedBy: "/")
        if topicComponents.count != 3 {
            NSLog("Message.topic has unsupported format: \(message.topic)")
            return
        }
        
        deviceID = topicComponents[0]
        serviceID = topicComponents[1]
        characteristicID = topicComponents[2]
        
        self.processor.startScanning(deviceID: deviceID,
                                      service: serviceID,
                                      characteristic: characteristicID,
                                      delegate: delegate)
    }
    
    public func disconnect() {
        self.processor.disconnect()
    }
}

