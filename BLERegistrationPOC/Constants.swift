//
//  Constants.swift
//  BLERegistrationPOC
//
//  Created by Alain Juridico on 9/12/17.
//  Copyright © 2017 SPR Inc. All rights reserved.
//

import Foundation

class Constants {
    static let deviceNameKey: String = "deviceNameKey"
    static let deviceIDKey: String = "deviceIDKey"
    static let serviceIDKey: String = "serviceIDKey"
    static let characteristicIDKey: String = "characteristicIDKey"
    static let lastCharacteristicValueKey: String = "lastCharacteristicValueKey"
    static let lastNotificationReceivedDateKey: String = "lastNotificationReceivedDateKey"
}
