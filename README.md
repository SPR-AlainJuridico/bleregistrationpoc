# BLE Registration POC
# 
This project demonstrates the ability to register for change notifications to a
given characteristic of a given service of a particular BLE device. It also
demonstrates state preservation and restoration. We are able to continue to
receive characteristic change notifications from a BLE device while in the
background as well as when the app is terminated due to memory pressure. The
notifications are processed in real-time in both cases. We have implemented iOS
local notifications that will fire when processing a BLE characteristic changes
while the app is NOT active(i.e. backgrounded or terminated) which shows that
the characteristic changes are processed as they are received. The local iOS
notifications are suppressed to display a maximum of 1 per 10 seconds as not to
flood the device.

One caviate to to state preservation and restoration is that when your app is
re-launched to handle the BLE notification, you only have a short amount of
time(about 10 seconds) to do what you need to do, so keep it short and sweet.


## Classes
## 
## 
1. BLEMessage

The registration begins with a BLEMessage which is a struct that only contains a
single String property called "topic". A topic can be thought of in terms of a
URI. In this case, the expected format of the topic is: /(device ID)/(service
ID)/(characteristic ID). The BLEMessage struct is defined in the BLEBroker
class.

2. BLEBroker

The BLEBroker is where the registration begins. The class contains a subscribe
method used to subscribe to a given topic and a disconnect method. The subscribe
method takes a BLEMessage and a BLEDelegate used to receive callbacks about
specified BLE operations.

3. Processor

The Processor class handles all of the BLE interaction as well as sending the
iOS local notifications. It acts as the central manager of the BLE system and
contains methods to begin scanning, stop scanning and disconnect from
peripheral.


This project is a native iOS app, created using Xcode 8.3.3, written in Swift
3.1 and targeting iOS 9.0 but later changed over to target iOS 10.


## BLE Device
## 
## 
The BLE device we used to develop this app is the Bluefruit LE
Friend(https://www.adafruit.com/product/2267). The device has a single service
with a characteristic set to notify upon value change. We connect to this device
via terminal and can change the characteristic value at will which will trigger
the device to broadcast the value change notification.


## Getting Started
## 
## 
1. Plug the Bluefruit LE Friend device into USB 

2. Open a terminal window and connect to the device. - // TODO: Add instructions to connect to BLE device via terminal and confirm

3. Clone the repository - git clone https://bitbucket.org/SPR-AlainJuridico/bleregistrationpoc.git 

4. Open `BLERegistrationPOC.xcodeproj` 

5. Build and run on an iOS 10 or later device 

6. Tap the connect button to connect to the Bluefruit device via bluetooth 

7. Go back to terminal and make a change to the characteristic



## Assumptions
## 
## 
- This app assumes that the characteristic that we are subscribing to is
configured to broadcast notifications upon value change.

- This app assumes that you will connect to a single BLE device and subscribe to
a single characteristic's change notification.



## Next Steps
## 
## 
- Local notifications on iOS 11 device while app has been terminated is not
currently working. 
- Remove any local iOS notification code for iOS 9. 
- Set up
to maintain connection and receive characteristic notification from multiple BLE
devices. 
- Possibly set up CI




